# cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../alfred)
    message(STATUS " === Found AlfredHelper Folder")
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../alfred
                     "${CMAKE_BINARY_DIR}/AlfredHelper-build")
    return()
endif(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../alfred)

find_program(GIT_EXECUTABLE git)
if(NOT GIT_EXECUTABLE)
    message(FATAL_ERROR "\n***Couldn't find git executable.\n")
endif()

set(alfie_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/alfred_helper)
set(alfie_BIN_DIR ${alfie_PREFIX}/buildme)

if(NOT EXISTS ${alfie_PREFIX})
    
    message(STATUS "*** Will clone AlfredHelper repo.")
    set(cmd ${GIT_EXECUTABLE})
    set(_args "clone" "git@bitbucket.org:listboss/cpp-alfred"
        "${alfie_PREFIX}")
    set(WD ${CMAKE_CURRENT_BINARY_DIR})
    ExecuteProcess(${cmd} ${WD} ${_args})

endif(NOT EXISTS ${alfie_PREFIX})

add_subdirectory(${alfie_PREFIX} ${alfie_BIN_DIR})
