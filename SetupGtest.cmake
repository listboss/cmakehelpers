if (Gtest_LIBRARIES_RELEASE AND Gtest_LIBRARIES_DEBUG AND Gtest_INCLUDE_DIR)
    if(extern_gtest)
        message(STATUS
            "*** Using locally built Gtest for ${PROJECT_NAME}:\n"
            "    ${Gtest_LIBRARIES_RELEASE}\n"
            "    ${Gtest_LIBRARIES_DEBUG}\n"
            "    ${Gtest_INCLUDE_DIR}")
    elseif()
        message(STATUS
            "*** Using Gtest for ${PROJECT_NAME} from:\n"
            "    ${Gtest_LIBRARIES_RELEASE}\n"
            "    ${Gtest_LIBRARIES_DEBUG}\n"
            "    ${Gtest_INCLUDE_DIR}")
    endif()
    return()
endif(Gtest_LIBRARIES_RELEASE AND Gtest_LIBRARIES_DEBUG AND Gtest_INCLUDE_DIR)


set(Gtest_LIB_DIR ""
    CACHE PATH "Path with both release and debug version of Gtest.")

# Force building local Gtest?
if (Gtest_FORCE_LOCAL_BUILD)
    include(${cmh_dir}/DownloadGtest.cmake)
    return()
endif()


########### Find Gtest libraries
find_library(Gtest_LIBRARIES_RELEASE
    NAMES gtest
    HINTS ${Gtest_LIB_DIR}
    PATHS $ENV{HOME}/local/lib
    DOC "Google's unit test library for Release mode"
)
find_library(Gtest_LIBRARIES_DEBUG
    NAMES gtest_d
    HINTS ${Gtest_LIB_DIR}
    PATHS $ENV{HOME}/local/lib
    DOC "Google's unit test library for Debug mode"
)

if((NOT Gtest_LIBRARIES_DEBUG) OR (NOT Gtest_LIBRARIES_RELEASE))
    message(WARNING "Couldn't find Gtest libraries.")
    # Download and build Gtest
    include(${cmh_dir}/DownloadGtest.cmake)
    return()
endif()

#### Get both debug and release libraries paths
set(Gtest_LIB_DIR "")
get_filename_component(Gtest_LIB_DIR1 ${Gtest_LIBRARIES_DEBUG} PATH)
get_filename_component(Gtest_LIB_DIR2 ${Gtest_LIBRARIES_RELEASE} PATH)
list(APPEND Gtest_LIB_DIR ${Gtest_LIB_DIR1})
list(APPEND Gtest_LIB_DIR ${Gtest_LIB_DIR2})

#### Get header/library for gtest
get_filename_component(inc_guess ${Gtest_LIB_DIR2} PATH)
find_path(Gtest_INCLUDE_DIR
    NAMES gtest/gtest.h
    PATHS ${inc_guess}/include ${Gtest_INCLUDE_DIR} $ENV{HOME}/local/include
    DOC "Header dir containing Google's unit test library"
    CMAKE_ROOT_PATH_BOTH)

if(NOT EXISTS ${Gtest_INCLUDE_DIR}/gtest/gtest.h)
    message(FATAL_ERROR "Can't find gtest/gtest.h"
    "\nSet Gtest_INCLUDE_DIR to the folder containing gtest header files."
    "\nor set Gtest_FORCE_LOCAL_BUILD to true to build Gtest locally.")
endif()

# set(Gtest_LIBRARIES_RELEASE ${Gtest_LIBRARIES_RELEASE}
#     CACHE FILEPATH "Release version of Gtest libs.")
# set(Gtest_LIBRARIES_DEBUG ${Gtest_LIBRARIES_DEBUG}
#     CACHE FILEPATH "Debug version of Gtest libs.")

message(STATUS
    "*** Found Gtest:\n"
    "    ${Gtest_LIBRARIES_RELEASE}\n"
    "    ${Gtest_LIBRARIES_DEBUG}\n"
    "    ${Gtest_INCLUDE_DIR}")


unset(inc_guess)  #unset temporary variable.
unset(Gtest_LIB_DIR1)
unset(Gtest_LIB_DIR2)
