set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${cppnetlib_DIR})
find_package(cppnetlib CONFIG QUIET)

if(CPPNETLIB_LIBRARIES)
    if (NOT extern_cppnetlib)
        message(STATUS "*** Found a cpp-netlib on your machine.")
        set(extern_cppnetlib 0
            CACHE INTERNAL "did we download and compile cppnetlib")
        return()
    else()
        message(STATUS "*** Using locally built cpp-netlib")
    endif()
endif(CPPNETLIB_LIBRARIES)

if(NOT extern_cppnetlib)
    message(STATUS "*** Will download & build cpp-netlib.")
endif()

# message(STATUS "CMAKE_CXX_FLAGS_RELEASE: ${CMAKE_CXX_FLAGS_RELEASE}")


set(cppnetlib_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/cpp_netlib)
set(cppnetlib_INSTALL_DIR ${cppnetlib_PREFIX}/Installation)
set(cppnetlib_CMAKE_ARGS
            -DCMAKE_BUILD_TYPE:STRING=Release
            -DCMAKE_INSTALL_PREFIX=${cppnetlib_INSTALL_DIR}
            -DBOOST_LIBRARYDIR=${Boost_LIBRARY_DIRS}
            -DBOOST_INCLUDEDIR=${Boost_INCLUDE_DIRS}
            # -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
            # -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
            # -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
)

set(cppnetlib_BIN_DIR ${cppnetlib_PREFIX}/Build)
set(cppnetlib_SRC_DIR ${cppnetlib_PREFIX}/Source)

if(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)

    # message(STATUS "---> modifying the msvc flags for cpp-netlib")
    set(_cppnetlib_flags "/TP /W3")
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    set(_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${_cppnetlib_flags}")
    set(_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${_cppnetlib_flags}")

    set(cppnetlib_CMAKE_ARGS "${cppnetlib_CMAKE_ARGS}"
        "-DCMAKE_CXX_FLAGS_RELEASE:STRING=${_CXX_FLAGS_RELEASE}")
    set(cppnetlib_CMAKE_ARGS "${cppnetlib_CMAKE_ARGS}"
        "-DCMAKE_CXX_FLAGS_DEBUG:STRING=${_CXX_FLAGS_DEBUG}")

endif(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)

# message(STATUS "cppnetlib_CMAKE_ARGS: ${cppnetlib_CMAKE_ARGS}")

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Debug")
endif()

set(CPP_NETLIB_BUILD_TYPE ${CMAKE_BUILD_TYPE}
    CACHE STRING "Config mode for CPP-NETLIB" FORCE)

# message(STATUS "---<<<<<\n${CMAKE_COMMAND}"
#         "-DCMAKE_BINARY_DIR=${cppnetlib_BIN_DIR} ${cppnetlib_CMAKE_ARGS}"
#         "-G ${CMAKE_GENERATOR} ${cppnetlib_SRC_DIR}")

include(ExternalProject)
ExternalProject_Add(cpp_netlib_extern
    PREFIX ${cppnetlib_PREFIX}
    GIT_REPOSITORY git://github.com/cpp-netlib/cpp-netlib.git
    GIT_TAG 0.11-devel
    CONFIGURE_COMMAND ${CMAKE_COMMAND}
        -DCMAKE_BINARY_DIR=${cppnetlib_BIN_DIR} ${cppnetlib_CMAKE_ARGS}
        -G ${CMAKE_GENERATOR} ${cppnetlib_SRC_DIR}
    BUILD_COMMAND ${CMAKE_COMMAND}
        -Dbin_dir=${cppnetlib_BIN_DIR}
        -Dconfig=${CPP_NETLIB_BUILD_TYPE}
        -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/Build_Ind_Targets_CPP-NETLIB.cmake
    INSTALL_COMMAND ""
    BINARY_DIR ${cppnetlib_BIN_DIR}
    SOURCE_DIR ${cppnetlib_SRC_DIR}
    DOWNLOAD_DIR ${cppnetlib_PREFIX}/tmp
    STAMP_DIR ${cppnetlib_PREFIX}/tmp/Stamps
    # CMAKE_ARGS ${cppnetlib_CMAKE_ARGS}
)

set(prefix ${CMAKE_STATIC_LIBRARY_PREFIX})
set(suffix ${CMAKE_STATIC_LIBRARY_SUFFIX})

string(REGEX MATCH "Visual Studio.*" _msvs_gen ${CMAKE_GENERATOR})
if(NOT _msvs_gen)
    message(STATUS "CMAKE_GENERATOR: ${CMAKE_GENERATOR}")
    set(CPP_NETLIB_BUILD_TYPE "")
endif()

if(WIN32)
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_BIN_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
elseif(APPLE)
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_BIN_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
else()
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_BIN_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
endif(WIN32)

string(REGEX MATCH "Visual Studio.*" _msvs_gen ${CMAKE_GENERATOR})
if(_msvs_gen)
    set(_build_folder_d "Debug")
    set(_build_folder_r "Release")
else()
    set(_build_folder_r "")
    set(_build_folder_d "")
endif()

add_custom_command(TARGET cpp_netlib_extern POST_BUILD
    COMMAND "${CMAKE_COMMAND}" -E make_directory ${cppnetlib_INSTALL_DIR}
    COMMAND "${CMAKE_COMMAND}" -E copy
        ${CPPNETLIB_LIB_DIR}/${prefix}cppnetlib-uri${suffix}
        ${cppnetlib_INSTALL_DIR}
    COMMAND "${CMAKE_COMMAND}" -E copy
        ${CPPNETLIB_LIB_DIR}/${prefix}cppnetlib-client-connections${suffix}
        ${cppnetlib_INSTALL_DIR}
    COMMENT "Copying cpp-netlib to better location.")

set(CPPNETLIB_INCLUDE_DIRS ${cppnetlib_SRC_DIR})
set(CPPNETLIB_LIBRARIES
    ${cppnetlib_INSTALL_DIR}/${prefix}cppnetlib-client-connections${suffix}
    ${cppnetlib_INSTALL_DIR}/${prefix}cppnetlib-uri${suffix}
)

# cpp-netlib will link against OpenSSL if found. We need to add OpenSSL
# to our target dependencies.
find_package(OpenSSL)
if(OPENSSL_FOUND)
    if(MSVC)
        message(STATUS "OPENSSL_LIBRARIES: ${OPENSSL_LIBRARIES}")
        string(REPLACE "MD.lib" "MT.lib" OPENSSL_LIBRARIES "${OPENSSL_LIBRARIES}")
        string(REPLACE "MDd.lib" "MTd.lib" OPENSSL_LIBRARIES "${OPENSSL_LIBRARIES}")
        message(STATUS "OPENSSL_LIBRARIES: ${OPENSSL_LIBRARIES}")
    endif(MSVC)
    set(CPPNETLIB_LIBRARIES ${CPPNETLIB_LIBRARIES} ${OPENSSL_LIBRARIES})
endif(OPENSSL_FOUND)

set(extern_cppnetlib 1 CACHE INTERNAL "Did we download and compile cppnetlib")
unset(prefix)
unset(suffix)
