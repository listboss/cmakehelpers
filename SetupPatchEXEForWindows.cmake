function(SetupPatchEXEForWindows dst batch_file patch_file)
# Download GNU patch for Windows and unzip it into ${dst}
# It will create a ${batch_file} so that
# user can call to apply ${patch_file}

    # message(STATUS "&###### ---> INPUT ARGS ARE:\n${dst}\n"
    #     "${batch_file}\n${patch_file}\n${original_file}")

    if(NOT patch_executable)

        if(NOT EXISTS ${dst})
            execute_process(
                COMMAND "${CMAKE_COMMAND}" -E make_directory ${dst}
            )
        endif(NOT EXISTS ${dst})

        if(NOT EXISTS ${dst}/patch.zip)
            set(url "http://downloads.sourceforge.net/project/gnuwin32/patch/2.5.9-7/patch-2.5.9-7-bin.zip?r=http%3A%2F%2Fgnuwin32.sourceforge.net%2Fpackages%2Fpatch.htm&ts=1393305516&use_mirror=iweb")
            message(STATUS "  Need to download patch.exe")
            file(DOWNLOAD
                ${url}
                ${dst}/patch.zip
                TIMEOUT 60 STATUS download_patch_STATUS
                LOG download_patch_log)

            list(GET download_patch_STATUS 0 _status)
            if(_status)
                list(GET download_patch_STATUS 1 _error)
                message(FATAL_ERROR "Couldn't download from ${url}."
                    "\n${_error}\n${download_patch_log}")
            endif(_status)
        endif(NOT EXISTS ${dst}/patch.zip)

        if(NOT EXISTS ${dst}/bin)
            message(STATUS "  Need to unzip patch.zip")
            # Unzip the downloaded file
            execute_process(
                COMMAND "${CMAKE_COMMAND}" -E tar xzf ${dst}/patch.zip
                WORKING_DIRECTORY ${dst}
                RESULT_VARIABLE patch_unzip_RESULT
                OUTPUT_VARIABLE patch_unzip_OUTPUT
                ERROR_VARIABLE patch_unzip_ERROR
            )

            if(patch_unzip_RESULT)
                message(FATAL_ERROR "Couldn't unzip ${CMAKE_CURRENT_BINARY_DIR}/patch.zip"
                    "\nError: ${patch_unzip_ERROR}. (${patch_unzip_RESULT})")
            endif()

            set(patch_executable "${dst}/bin/patch.exe"
                CACHE FILEPATH "patch.exe for Windows.")
        endif(NOT EXISTS ${dst}/bin)

    else()
        message(STATUS "  Using already setup patch.exe: ${patch_executable}")
    endif(NOT patch_executable)

    # Create a batch file
    if(NOT EXISTS ${batch_file})

        get_filename_component(_bat_name "${batch_file}" NAME)
        message(STATUS "  Need to create ${_bat_name}")

        get_filename_component(_bat_ext "${batch_file}" EXT)
        if(NOT ${_bat_ext} STREQUAL ".bat")
            message(FATAL_ERROR
                "Windows batch file name needs to have .bat ext.\n"
                "You provided: ${batch_file}")
        endif(NOT ${_bat_ext} STREQUAL ".bat")

        file(WRITE "${batch_file}"
            "${patch_executable} --binary --input=${patch_file} --strip=1"
        )
        message(STATUS "    done.")

    endif()

endfunction()
