# build cpp-netlib's 3 main librarries individually
cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

set(targets
    "cppnetlib-client-connections"
    "cppnetlib-uri")

foreach(tar ${targets})
    message(STATUS "    Building: ${tar}")
    execute_process(
        COMMAND
        ${CMAKE_COMMAND} --build ${bin_dir} --target ${tar} --config ${config}
        WORKING_DIRECTORY "${bin_dir}"
        RESULT_VARIABLE uri_build_step_RESULT
        OUTPUT_VARIABLE uri_build_step_OUTPUT
        ERROR_VARIABLE uri_build_step_ERROR
        OUTPUT_FILE ${bin_dir}/${tar}_build_error.log
    )

    if(${uri_build_step_RESULT})
        message(FATAL_ERROR "Couldn't build target ${tar} of cpp-netlib"
            "\nCheck the log file: ${bin_dir}/${tar}_build_error.log")
    endif(${uri_build_step_RESULT})

endforeach(tar ${targets})

