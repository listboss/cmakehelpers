cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

if(rapidxml_INCLUDE_DIR)
    message(STATUS "*** Found rapidxml: ${rapidxml_INCLUDE_DIR}")
    return()
endif(rapidxml_INCLUDE_DIR)

message(STATUS "*** Setting up patched RapidXML.")

set(rapidxml_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/rapidxml_patched)

# Copy our patched version of rapidxml
if(NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/rapidxml_patched.zip)
    set(url file://${CMAKE_CURRENT_SOURCE_DIR}/rapidxml_patched.zip)
    file(
        DOWNLOAD ${url}
        ${rapidxml_PREFIX}.zip
        TIMEOUT 120 STATUS download_rapidxml_STATUS
        LOG ${CMAKE_CURRENT_BINARY_DIR}/rapidxml_download.log
        EXPECTED_MD5 61b1678107444cd2375c20e8c9ead796
    )

    list(GET download_rapidxml_STATUS 0 _status)
    if(_status)
        list(GET download_rapidxml_STATUS 1 _error)
        message(FATAL_ERROR "Couldn't download Gtest zip file."
            "\nError: ${_error}\n (${url})")
    endif(_status)

endif(NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/rapidxml_patched.zip)

execute_process(
    COMMAND
        "${CMAKE_COMMAND}" -E tar xzf ${rapidxml_PREFIX}.zip
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

# execute_process(
#     COMMAND "${CMAKE_COMMAND}" -E rename
#         ${rapidxml_PREFIX}-1.13 ${rapidxml_PREFIX}
#     COMMAND "${CMAKE_COMMAND}" -E remove_directory ${rapidxml_PREFIX}-1.13
#     WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
#     RESULT_VARIABLE variable_RESULT
#     OUTPUT_VARIABLE variable_OUTPUT
#     ERROR_VARIABLE variable_ERROR)

set(rapidxml_INCLUDE_DIR ${rapidxml_PREFIX}
    CACHE INTERNAL "rapidxml include directory.")
