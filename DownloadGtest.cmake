cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

if(NOT extern_gtest)
    message(STATUS "*** Downloading & building Gtest.")
endif()

set(debug_postfix "_d")
set(gtest_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gtest-1.7.0)
set(gtest_SOURCE_DIR ${gtest_PREFIX})

set(gtest_CMAKE_ARGS_Release
    "-DCMAKE_BUILD_TYPE:STRING=Release")
set(gtest_CMAKE_ARGS_Debug
    "-DCMAKE_BUILD_TYPE:STRING=Debug" "-DCMAKE_DEBUG_POSTFIX:STRING=${debug_postfix}")

if(MSVC)

    set(_gtest_flags "/TP /W3")
    if(NOT USE_SHARED_RUNTIME_WINDOWS)
        set(_foo "/MT")
    else()
        set(_foo "/MD")
    endif(NOT USE_SHARED_RUNTIME_WINDOWS)

    set(_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${_gtest_flags} ${_foo}")
    set(_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${_gtest_flags} ${_foo}d")

    set(gtest_CMAKE_ARGS_Release "${gtest_CMAKE_ARGS_Release}"
        "-DCMAKE_CXX_FLAGS_RELEASE:STRING=${_CXX_FLAGS_RELEASE}")
    set(gtest_CMAKE_ARGS_Debug "${gtest_CMAKE_ARGS_Debug}"
        "-DCMAKE_CXX_FLAGS_DEBUG:STRING=${_CXX_FLAGS_DEBUG}")

endif(MSVC)

set(gtest_BIN_DIR_Release ${gtest_PREFIX}/Build)
set(gtest_BIN_DIR_Debug ${gtest_PREFIX}/Build_DEBUG)

execute_process(
    COMMAND ${CMAKE_COMMAND} -E make_directory ${gtest_PREFIX}
    COMMAND ${CMAKE_COMMAND} -E make_directory ${gtest_BIN_DIR_Release}
    COMMAND ${CMAKE_COMMAND} -E make_directory ${gtest_BIN_DIR_Debug}
)


# Download the zip file from google code
set(url http://googletest.googlecode.com/files/gtest-1.7.0.zip)
if(NOT EXISTS ${gtest_PREFIX}.zip)
    file(DOWNLOAD ${url}
        ${gtest_PREFIX}.zip
        TIMEOUT 120 STATUS download_gtest_STATUS
        LOG ${CMAKE_CURRENT_BINARY_DIR}/download.log
        EXPECTED_HASH SHA1=f85f6d2481e2c6c4a18539e391aa4ea8ab0394af
    )
endif()

# Check the download status
list(GET download_gtest_STATUS 0 _status)
if(_status)
    list(GET download_gtest_STATUS 1 _error)
    message(FATAL_ERROR "Couldn't download Gtest zip file."
        "\nError: ${_error}")
endif(_status)

# Unzip the downloaded file
set(cmd ${CMAKE_COMMAND})
set(_args "-E" "tar" "xzf" "${gtest_PREFIX}.zip")
set(WD ${CMAKE_CURRENT_BINARY_DIR})
ExecuteProcess(${cmd} ${WD} ${_args})

# Patch the cmake file so under windows Gtest is built with /MD flags
# instead of /MT
if(MSVC AND USE_SHARED_RUNTIME_WINDOWS)
    include(${PROJECT_BINARY_DIR}/cmake_helpers/SetupPatchEXEForWindows.cmake)

    set(patch_prefix ${CMAKE_CURRENT_BINARY_DIR}/gnu_patch)
    SetupPatchEXEForWindows(${patch_prefix}
        "${CMAKE_CURRENT_BINARY_DIR}/patch_gtest_cxx_flags.bat"
        "${PROJECT_BINARY_DIR}/cmake_helpers/gtest_cmake_shared_runtime.patch"
        "${gtest_SOURCE_DIR}/cmake/internal_utils.cmake")

    set(patch_working_dir ${gtest_SOURCE_DIR})
    set(gtest_patch_cmd "${CMAKE_CURRENT_BINARY_DIR}/patch_gtest_cxx_flags.bat"
        CACHE FILEPATH "Batch file location to run patch.exe" FORCE)

    # check if the file has already been patched
    file(SHA1 "${gtest_SOURCE_DIR}/cmake/internal_utils.cmake" gtest_internal_sha1)
    set(_internal_sha1_patched 2e8b3e504579291ffa34e3ceb534deb98446e93b)


    if(NOT ${gtest_internal_sha1} STREQUAL ${_internal_sha1_patched})

        message(STATUS "  Patching Gtest to use shared runtime.")
        execute_process(
            COMMAND ${gtest_patch_cmd}
            WORKING_DIRECTORY ${patch_working_dir}
            TIMEOUT 60
            RESULT_VARIABLE patch_gtest__RESULT
            OUTPUT_VARIABLE patch_gtest__OUTPUT
            ERROR_VARIABLE patch_gtest__ERROR)

        if(patch_gtest__RESULT OR patch_gtest__ERROR)
            message(FATAL_ERROR "Couldn't patch cpp-netlib."
                "\n${patch_gtest__OUTPUT}"
                "\n${patch_gtest__ERROR}. (${patch_gtest__RESULT})")
        endif()

    else()
        message(STATUS "  Gtest has already been patched.")
    endif()

endif(MSVC AND USE_SHARED_RUNTIME_WINDOWS)

# configure Gtest project
set(configs "Debug" "Release")
foreach(config ${configs})
    message(STATUS "Gtest config: ${config}")

    execute_process(
        COMMAND "${CMAKE_COMMAND}" -E chdir ${gtest_BIN_DIR_${config}}
            "${CMAKE_COMMAND}" ${gtest_CMAKE_ARGS_${config}} -G "${CMAKE_GENERATOR}" ..
        WORKING_DIRECTORY ${gtest_BIN_DIR_${config}}
        TIMEOUT 60
        RESULT_VARIABLE gtest_config_RESULT
        OUTPUT_VARIABLE gtest_config_OUTPUT
        ERROR_VARIABLE gtest_config_ERROR
    )
    if(gtest_config_RESULT)
        message(FATAL_ERROR
            "Couldn't configure ${config} config of Gtest"
            "\nError: ${gtest_config_ERROR}\n${gtest_config_RESULT}\n")
    endif()

    message(STATUS "Gtest build: ${config}")
    set(cmd ${CMAKE_COMMAND})
    set(_args "--build" "${gtest_BIN_DIR_${config}}" "--config" "${config}")
    set(WD ${gtest_BIN_DIR_${config}})
    ExecuteProcess(${cmd} ${WD} ${_args})

endforeach()


## Copy debug libraries to proper directory.
# construct proper name of libs
set(prefix ${CMAKE_STATIC_LIBRARY_PREFIX})
set(suffix ${CMAKE_STATIC_LIBRARY_SUFFIX})

set(_gtest_name ${prefix}gtest${suffix})

set(_gtest_d_name ${prefix}gtest${debug_postfix}${suffix})
set(_gtest_main_d_name ${prefix}gtest_main${debug_postfix}${suffix})

string(REGEX MATCH "(Visual Studio.*|Xcode)" _IDE ${CMAKE_GENERATOR})
if(_IDE)
    set(_build_folder_d "Debug")
    set(_build_folder_r "Release")
    message(STATUS "CMAKE_GENERATOR: ${CMAKE_GENERATOR}")
else()
    set(_build_folder_r "")
    set(_build_folder_d "")
endif()

message(STATUS "Gtest copying libraries.")
execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy
            ${gtest_BIN_DIR_Debug}/${_build_folder_d}/${_gtest_d_name}
            ${gtest_BIN_DIR_Release}/${_build_folder_r}/${_gtest_d_name}
    COMMAND ${CMAKE_COMMAND} -E copy
            ${gtest_BIN_DIR_Debug}/${_build_folder_d}/${_gtest_main_d_name}
            ${gtest_BIN_DIR_Release}/${_build_folder_r}/${_gtest_main_d_name}
    WORKING_DIRECTORY ${gtest_BIN_DIR_Debug}/${_build_folder_d}
    RESULT_VARIABLE gtest_copylib_RESULT
    OUTPUT_VARIABLE gtest_copylib_OUTPUT
    ERROR_VARIABLE gtest_copylib_ERROR
)

if(gtest_copylib_RESULT)
    message(FATAL_ERROR "Couldn't copy Gtest libs to proper location."
        "\nError: ${gtest_copylib_ERROR}. (${gtest_copylib_RESULT})")
endif()

set(extern_gtest 1 CACHE INTERNAL "did we download and compile Gtest")

set(Gtest_INCLUDE_DIR ${gtest_SOURCE_DIR}/include
    CACHE PATH "Header dir containing Google's unit test library")

set(Gtest_LIBRARIES_RELEASE
    ${gtest_BIN_DIR_Release}/${_build_folder_r}/${_gtest_name}
    CACHE FILEPATH "Google's unit test library for Debug mode" FORCE)
set(Gtest_LIBRARIES_DEBUG
    ${gtest_BIN_DIR_Release}/${_build_folder_r}/${_gtest_d_name}
    CACHE FILEPATH "Google's unit test library for Release mode" FORCE)

list(APPEND Gtest_LIB_DIR ${Gtest_LIBRARIES_RELEASE})
list(APPEND Gtest_LIB_DIR ${Gtest_LIBRARIES_DEBUG})

