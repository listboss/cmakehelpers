# Ex: SetCompileFlags(mytarget "${flags}")
# Set the COMPILE_FLAGS for the 'target'
function(SetCompileFlags target)
    if(${ARGC} EQUAL 1)
        message(WARNING "  No compiler flag was sent to SetCompileFlags!")
        return()
    else(${ARGC} EQUAL 1)
        set(flags ${ARGN})
    endif(${ARGC} EQUAL 1)

    foreach(f ${flags})
        if(newflags)
            set(newflags "${newflags} ${f}")
        else()
            set(newflags "${f}")
        endif(newflags)
    endforeach()

    get_property(Temp TARGET ${target} PROPERTY COMPILE_FLAGS)

    if("${Temp}" STREQUAL "NOTFOUND" OR "${Temp}" STREQUAL "" OR NOT Temp)
        set(Temp ${newflags})
    else()
        set(Temp "${Temp} ${newflags}")
    endif()

    message(STATUS "flags: '${Temp}' for target: \"${target}\"")

    set_target_properties(${target}
        PROPERTIES
        COMPILE_FLAGS "${Temp}")

ENDFUNCTION()

function(message_header)
  if(${ARGC} EQUAL 0)
    set(msg "")
  else()
    set(msg ${ARGN})
  endif()

  message(STATUS "")
  message(STATUS "")
  message(STATUS ${msg})
  message(STATUS "")
  message(STATUS "")

ENDFUNCTION()

function(ExecuteProcess cmd WD)

    execute_process(
        COMMAND ${cmd} ${ARGN}
        WORKING_DIRECTORY ${WD}
        TIMEOUT 120
        RESULT_VARIABLE process_var_RESULT
        OUTPUT_VARIABLE process_var_OUTPUT
        ERROR_VARIABLE process_var_ERROR
        OUTPUT_FILE ${WD}/_process_o.log
        ERROR_FILE ${WD}/_process_e.log)

    if(process_var_RESULT OR process_var_ERROR)
        message(FATAL_ERROR "Error in running ${cmd}"
            "\n${process_var_RESULT}\n"
            "(${process_var_ERROR}\n${process_var_OUTPUT})"
            "\nCheck the log files: ${WD}/_process_{o,e}.log")
    endif()

ENDFUNCTION()

function(CheckGCCVersion major_minor flag)
# Usage
# CheckGCCVersion(4.3 st)
# message(STATUS "st: ${st}")

    if(NOT ${CMAKE_C_COMPILER_ID} MATCHES GNU)
        set(${flag} FALSE PARENT_SCOPE)
        message(WARNING "Not a GNU compiler: ${CMAKE_C_COMPILER_ID}")
        return()
    endif()

    execute_process(
        COMMAND ${CMAKE_C_COMPILER} -dumpversion
        OUTPUT_VARIABLE GCC_VERSION)

    string(REGEX REPLACE "(.*)\n" "\\1" GCC_VERSION ${GCC_VERSION})

    if (GCC_VERSION VERSION_GREATER ${major_minor}} OR
        GCC_VERSION VERSION_EQUAL ${major_minor}})
        message(STATUS "GCC Version >= ${major_minor}")
        set(${flag} TRUE PARENT_SCOPE)
    else()
        message(WARNING "GCC Version < ${major_minor} (${GCC_VERSION}).")
        set(${flag} FALSE PARENT_SCOPE)
    endif()

ENDFUNCTION()

macro(fix_default_compiler_settings)
  if (MSVC)
    # For MSVC, CMake sets certain flags to defaults we want to override.
    # This replacement code is taken from sample in the CMake Wiki at
    # http://www.cmake.org/Wiki/CMake_FAQ#Dynamic_Replace.
    foreach (flag_var
             CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
             CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO)
      if (NOT BUILD_SHARED_LIBS)
        # When a project is built as a shared library, it should also use
        # shared runtime libraries.  Otherwise, it may end up with multiple
        # copies of runtime library data in different modules, resulting in
        # hard-to-find crashes. When it is built as a static library, it is
        # preferable to use CRT as static libraries, as we don't have to rely
        # on CRT DLLs being available. CMake always defaults to using shared
        # CRT libraries, so we override that default here.
        # message(STATUS ": before-> flag_var: ${flag_var}\n${${flag_var}}")
        string(REPLACE "/MD" "-MT" ${flag_var} "${${flag_var}}")
        # message(STATUS ": after-> flag_var: ${flag_var}\n${${flag_var}}")

      endif()
    endforeach()
  endif()
endmacro()
