# cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../WebService)
    message(STATUS " === Found WebService Folder")
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../WebService
                     "${CMAKE_BINARY_DIR}/WebServiceLib-build")
    return()
endif(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../WebService)

find_program(GIT_EXECUTABLE git)
if(NOT GIT_EXECUTABLE)
    message(FATAL_ERROR "\n***Couldn't find git executable.\n")
endif()

set(webservice_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/webservice)
set(webservice_BIN_DIR ${webservice_PREFIX}/buildme)

if(NOT EXISTS ${webservice_PREFIX})
    
    message(STATUS "*** Will clone WebServiceLib repo.")
    set(cmd ${GIT_EXECUTABLE})
    set(_args "clone" "git@bitbucket.org:listboss/WebService"
        "${webservice_PREFIX}")
    set(WD ${CMAKE_CURRENT_BINARY_DIR})
    ExecuteProcess(${cmd} ${WD} ${_args})

endif(NOT EXISTS ${webservice_PREFIX})

add_subdirectory(${webservice_PREFIX} ${webservice_BIN_DIR})
