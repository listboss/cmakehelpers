# cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${cppnetlib_DIR})
find_package(cppnetlib CONFIG QUIET)

if(CPPNETLIB_LIBRARIES)
    if (NOT built_cppnetlib_locally)
        message(STATUS
            "*** Found a cpp-netlib on your machine."
            "\n        ${cppnetlib_DIR}")
        set(built_cppnetlib_locally 0
            CACHE INTERNAL "Did we download and compile cppnetlib" FORCE)
        return()
    else()
        message(STATUS "*** Using locally built cpp-netlib")
    endif()
endif(CPPNETLIB_LIBRARIES)

if(NOT built_cppnetlib_locally)
    message(STATUS "*** Will clone & build cpp-netlib.")
endif()

set(cppnetlib_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/cpp_netlib)
set(cppnetlib_build_DIR ${cppnetlib_PREFIX}/Build)
set(cppnetlib_SRC_DIR ${cppnetlib_PREFIX})

set(CPP_NETLIB_BUILD_TYPE ${CMAKE_BUILD_TYPE}
    CACHE STRING "Config mode for CPP-NETLIB" FORCE)


find_program(GIT_EXECUTABLE git)
if(NOT GIT_EXECUTABLE)
    message(FATAL_ERROR "\n***Couldn't find git executable.\n")
endif()


# Clone step
if(NOT EXISTS ${cppnetlib_SRC_DIR})

    message(STATUS "  clonning cpp-netlib")
    set(cmd ${GIT_EXECUTABLE})
    set(_args "clone" "git://github.com/cpp-netlib/cpp-netlib.git"
        "-b" "0.11-devel" "${cppnetlib_SRC_DIR}")
    set(WD ${CMAKE_CURRENT_BINARY_DIR})
    ExecuteProcess(${cmd} ${WD} ${_args})

    # Checkout the commit that we can build against
    set(_args "checkout" "5c8d9ab0339eb06f1115e24cd054f6b21c8bf233")
    set(WD ${cppnetlib_PREFIX})
    ExecuteProcess(${cmd} ${WD} ${_args})


endif(NOT EXISTS ${cppnetlib_SRC_DIR})

# Patch step
if(WIN32)

    include(${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/SetupPatchEXEForWindows.cmake)

    set(patch_prefix ${CMAKE_CURRENT_BINARY_DIR}/gnu_patch)
    SetupPatchEXEForWindows(${patch_prefix}
        "${CMAKE_CURRENT_BINARY_DIR}/run_patch.bat"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/PATCH_cpp-netlib_CMake_Files.patch"
        "${cppnetlib_PREFIX}/CMakeLists.txt")

    set(patch_working_dir ${cppnetlib_SRC_DIR})
    set(patch_cmd "${CMAKE_CURRENT_BINARY_DIR}/run_patch.bat"
        CACHE FILEPATH "Batch file location to run patch.exe" FORCE)

else() # Not WIN32
    if(NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/run_patch.sh)

        file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/run_patch.sh"
            "patch -p1 < "
            "${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/PATCH_cpp-netlib_CMake_Files.patch"
        )
        file(COPY "${CMAKE_CURRENT_BINARY_DIR}/cmake_helpers/run_patch.sh"
            DESTINATION "${CMAKE_CURRENT_BINARY_DIR}"
            FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
            GROUP_READ GROUP_EXECUTE
        )

    endif(NOT EXISTS ${CMAKE_CURRENT_BINARY_DIR}/run_patch.sh)

    set(patch_working_dir ${cppnetlib_SRC_DIR})
    set(patch_cmd "${CMAKE_CURRENT_BINARY_DIR}/run_patch.sh"
        CACHE FILEPATH "Shell file location to run patch" FORCE)

endif(WIN32)

# check if the file has already been patched
file(SHA1 ${cppnetlib_SRC_DIR}/CMakeLists.txt cppnetlib_cmakelist_sha1)
set(_cmakelists_sha1_patched 2daeb10667d90beaba447c26eef1824bace51c9c)


if(NOT ${cppnetlib_cmakelist_sha1} STREQUAL ${_cmakelists_sha1_patched})

    message(STATUS "  Patching CMakeLists.txt to only build libraries.")
    set(cmd ${patch_cmd})
    set(WD ${patch_working_dir})
    ExecuteProcess(${cmd} ${WD} ${_args})

else()
    message(STATUS "  CMakeLists.txt has already been patched.")
endif()

# Include cpp-netlib project
add_subdirectory(${cppnetlib_SRC_DIR} ${cppnetlib_build_DIR})


# Setup variables for include and library locations.
set(prefix ${CMAKE_STATIC_LIBRARY_PREFIX})
set(suffix ${CMAKE_STATIC_LIBRARY_SUFFIX})

string(REGEX MATCH "(Visual Studio.*|Xcode)" _IDE ${CMAKE_GENERATOR})
if(NOT _IDE)
    set(CPP_NETLIB_BUILD_TYPE "")
endif()

if(WIN32)
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_build_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
elseif(APPLE)
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_build_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
else()
    set(CPPNETLIB_LIB_DIR
    "${cppnetlib_build_DIR}/libs/network/src/${CPP_NETLIB_BUILD_TYPE}")
endif(WIN32)


set(CPPNETLIB_INCLUDE_DIRS ${cppnetlib_SRC_DIR})
set(CPPNETLIB_LIBRARIES
    ${CPPNETLIB_LIB_DIR}/${prefix}cppnetlib-client-connections${suffix}
    ${CPPNETLIB_LIB_DIR}/${prefix}cppnetlib-uri${suffix}
)

# cpp-netlib will link against OpenSSL if found. We need to add OpenSSL
# to our target dependencies.
find_package(OpenSSL)
if(OPENSSL_FOUND)

    if(MSVC AND NOT USE_SHARED_RUNTIME_WINDOWS)
        # message(STATUS "OPENSSL_LIBRARIES: ${OPENSSL_LIBRARIES}")
        foreach(lib ${OPENSSL_LIBRARIES})
            string(REGEX REPLACE "lib/VC/(.*)(MD)(d?\\.lib)" "lib/VC/static/\\1MT\\3" output ${lib})
            list(APPEND _newlibs ${output})
            unset(output)
        endforeach()
        set(OPENSSL_LIBRARIES "${_newlibs}")
        # message(STATUS "OPENSSL_LIBRARIES: ${OPENSSL_LIBRARIES}")
    endif(MSVC AND NOT USE_SHARED_RUNTIME_WINDOWS)

    set(CPPNETLIB_LIBRARIES ${CPPNETLIB_LIBRARIES} ${OPENSSL_LIBRARIES})

endif(OPENSSL_FOUND)

set(built_cppnetlib_locally 1 CACHE INTERNAL "Did we download and compile cppnetlib")

unset(prefix)
unset(suffix)
unset(_IDE)
