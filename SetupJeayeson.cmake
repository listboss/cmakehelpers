cmake_policy(SET CMP0012 NEW)  # IF() statement behavior

find_program(GIT_EXECUTABLE git)
if (NOT GIT_EXECUTABLE)
    message(FATAL_ERROR "\n*** Couldn't find git executable.\n")
endif(NOT GIT_EXECUTABLE)

function(clone_jeayeson)

    # 'git clone' the jeayson from Github
    # SET(CMAKE_CURRENT_BINARY_DIR "/Volumes/Home/hamid/src/learn/cpp/WebService/build")
    SET(jeayeson_CLONED_DIR ${CMAKE_CURRENT_BINARY_DIR}/jeayeson)

    if (NOT EXISTS ${jeayeson_CLONED_DIR})
        message(STATUS "  Clonning jeayeson")
        SET(cmd ${GIT_EXECUTABLE})
        set(_args "clone" "git://github.com/jeaye/jeayeson"
            "-b" "master" "${jeayeson_CLONED_DIR}")
        set(WD ${CMAKE_CURRENT_BINARY_DIR})
        ExecuteProcess(${cmd} ${WD} ${_args})

        # Checkout the specific commit
        set(_args "checkout" "8b51beacbfa99a38fd617bb24bc420c7ee78f6ca")
        set(WD ${jeayeson_CLONED_DIR})
        ExecuteProcess(${cmd} ${WD} ${_args})
        message(STATUS "    done.")
    endif(NOT EXISTS ${jeayeson_CLONED_DIR})

    if (NOT EXISTS ${jeayeson_CLONED_DIR}/include/jeayeson/config.hpp)
        file(WRITE ${jeayeson_CLONED_DIR}/include/jeayeson/config.hpp
                "#pragma once
                namespace jeayeson
                {
                  /* To customize jeayeson internals, change these types
                   * to any other compatible types which suit your needs.
                   * Add any required #includes here.
                   */
                  template <>
                  struct config<config_tag>
                  {
                    using float_t = double;
                    using int_t = int64_t;
                    template <typename K, typename V>
                    using map_t = std::map<K, V>;
                  };
                }"
        )
    endif(NOT EXISTS ${jeayeson_CLONED_DIR}/include/jeayeson/config.hpp)


    set(Jeayeson_INCLUDE_DIR "${jeayeson_CLONED_DIR}/include"
        CACHE INTERNAL "Jeayeson include directory")

endfunction(clone_jeayeson)


# If Jeayeson include dir set by user, use it otherwise clone if can't find the header files
if (NOT Jeayeson_INCLUDE_DIR)
    message(STATUS "Setting up Jeayeson.")
    find_path(Jeayeson_INCLUDE_DIR jeayeson.hpp)
    if (NOT Jeayeson_INCLUDE_DIR-NOTFOUND)
        clone_jeayeson()
    else(NOT Jeayeson_INCLUDE_DIR)
        set(Jeayeson_INCLUDE_DIR "${Jeayeson_INCLUDE_DIR}/..")
        message(STATUS "${Jeayeson_INCLUDE_DIR}")
    endif(NOT Jeayeson_INCLUDE_DIR-NOTFOUND)
endif(NOT Jeayeson_INCLUDE_DIR)

# Check if required files can be found in the jeayeson_INCLUDE_DIR
if (NOT EXISTS ${Jeayeson_INCLUDE_DIR}/jeayeson/jeayeson.hpp OR
    NOT EXISTS ${Jeayeson_INCLUDE_DIR}/jeayeson/config.hpp)
    message(STATUS "****** Can't find Jeayeson header files. ******")
    message(FATAL_ERROR "** Jeayeson_INCLUDE_DIR is: '${Jeayeson_INCLUDE_DIR}'")
endif(NOT EXISTS ${Jeayeson_INCLUDE_DIR}/jeayeson/jeayeson.hpp OR
    NOT EXISTS ${Jeayeson_INCLUDE_DIR}/jeayeson/config.hpp)
message(STATUS "*** Found Jeayeson: ${Jeayeson_INCLUDE_DIR}")

